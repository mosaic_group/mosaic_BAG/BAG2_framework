# -*- coding: utf-8 -*-

"""This module provides functions needed to get Virtuoso to work with BAG.
"""

import os
import logging
import sys
import atexit
import signal
import socket
import argparse
import traceback

import bag.interface
import bag.io


def is_port_in_use(port: int) -> bool:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex(('localhost', port)) == 0


def run_skill_server(args):
    """Run the BAG/Virtuoso server."""
    error_msg = None
    server = None
    port_file = None
    port_number = None

    try:
        # process command line arguments
        min_port = args.min_port
        max_port = args.max_port
        # remove directory from port file name
        port_file = os.path.basename(args.port_file)
        log_file = args.log_file

        # create log file directory, and remove old log.
        if log_file is not None:
            log_file = os.path.abspath(log_file)
            log_dir = os.path.dirname(log_file)
            if not os.path.exists(log_dir):
                os.makedirs(log_dir)
            elif os.path.exists(log_file):
                os.remove(log_file)

        logging.basicConfig(filename=log_file, level=logging.INFO,
                            format='%(asctime)s %(levelname)-8s [%(module)-8s] %(message)s')
        logging.info("Started")

        # determine port file name
        if 'VIRTUOSO_DIR' not in os.environ:
            raise Exception('Environment variable VIRTUOSO_DIR not defined')
        work_dir = os.environ['VIRTUOSO_DIR']
        if not os.path.isdir(work_dir):
            raise Exception('VIRTUOSO_DIR = %s is not a directory' % work_dir)

        port_file = os.path.join(work_dir, port_file)

        # NOTE: Workaround for issue
        #       https://gitlab.com/mosaic_group/mosaic_BAG/BAG2_framework/-/issues/3
        #       Virtuoso sometimes restarts the IPC command, without any need.
        #       (for example after an ADEXL simulation).
        #       This would create a new process with a different BAG_server_port.txt,
        #       and break running generator scripts
        #       (their BagProjects ZMQ clients are still connected to the previous server port)
        if os.path.exists(port_file):
            with open(port_file, "r") as f:
                lines = f.readlines()
                if len(lines) == 1:
                    previous_port = lines[0].strip()
                    if previous_port.isdigit():
                        previous_port = int(previous_port)
                        if is_port_in_use(previous_port):
                            logging.warning(f"Port file exists, previous port {previous_port} is still reachable! "
                                            f"Therefore we bail...")
                            sys.exit(1)
                        else:
                            logging.info(f"Port file exists, but previous port {previous_port} is no longer reachable! "
                                         f"Therefore we continue...")

        # determine temp directory
        tmp_dir = None
        if 'BAG_TEMP_DIR' in os.environ:
            tmp_dir = os.environ['BAG_TEMP_DIR']
            if not os.path.isdir(tmp_dir):
                if os.path.exists(tmp_dir):
                    raise Exception('$BAG_TEMP_DIR = %s is not a directory' % tmp_dir)
                else:
                    os.makedirs(tmp_dir)

        # attempt to open port and start server
        router = bag.interface.ZMQRouter(min_port=min_port, max_port=max_port)
        server = bag.interface.SkillServer(router, sys.stdout, sys.stdin, tmpdir=tmp_dir)
        port_number = router.get_port()

        bag.io.write_file(port_file, '%r\n' % port_number)

        logging.info(f"Listening on port {port_number}")

        sys.stdout.write('BAG skill server has started.  Yay!\n')
        sys.stdout.flush()
        server.run()
    except Exception as ex:
        logging.exception("Caught exception")

        error_msg = 'bag server process error:\n%s\n' % str(ex)
        sys.stderr.write(error_msg)
        sys.stderr.flush()
    finally:
        logging.info("Finished")


def parse_command_line_arguments():
    """Parse command line arguments, then run the corresponding function."""

    desc = 'A Python program that performs tasks for virtuoso.'
    parser = argparse.ArgumentParser(description=desc)
    desc = 'Valid commands.  Supply -h/--help flag after the command name to learn more about the command.'
    sub_parsers = parser.add_subparsers(title='Commands', description=desc, help='command name.')

    desc = 'Run BAG skill server.'
    par2 = sub_parsers.add_parser('run_skill_server', description=desc, help=desc)

    par2.add_argument('min_port', type=int, help='minimum socket port number.')
    par2.add_argument('max_port', type=int, help='maximum socket port number.')
    par2.add_argument('port_file', type=str, help='file to write the port number to.')
    par2.add_argument('log_file', type=str, nargs='?', default=None,
                      help='log file name.')
    par2.set_defaults(func=run_skill_server)

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    parse_command_line_arguments()
