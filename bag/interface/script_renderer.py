# -*- coding: utf-8 -*-

"""
This module defines a base class
for rendering default schematic generator scripts non-primitive cells.
"""

from abc import ABC, abstractmethod


class ScriptRenderer(ABC):
    @abstractmethod
    def render_schematic_generator_script(self,
                                          lib_name: str,
                                          cell_name: str) -> str:
        """
        Callback for DbAccess to render the BagModules schematic generator script
        for newly imported schematics.
        As we use generator/testbench packages, we only import the schematic generator located in the package.

        Parameters
        ----------
        lib_name : str
            DB Library name
        cell_name
            DB Cell name

        Returns
        -------
        Rendered script as a string
        """
        pass
