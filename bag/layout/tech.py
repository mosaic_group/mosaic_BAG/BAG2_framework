# -*- coding: utf-8 -*-

from typing import List, Tuple, Union, Optional, Callable, Dict, Any, TYPE_CHECKING

from abc import ABCMeta, abstractmethod

from .core import TechInfo

if TYPE_CHECKING:
    from ..layout.util import BBox
    from ..layout.template import TemplateBase


class TechInfoConfig(TechInfo, metaclass=ABCMeta):
    """An implementation of TechInfo that implements most methods with a technology file."""
    def __init__(self,
                 config: Dict[str, Any],
                 tech_params: Dict[str, Any],
                 mos_entry_name: str = 'mos'):
        super().__init__(res=config['resolution'],
                         layout_unit=config['layout_unit'],
                         via_tech=config['tech_lib'],
                         process_params=tech_params)

        self.config = config
        self._mos_entry_name = mos_entry_name
        self.idc_temp = tech_params['layout']['em']['dc_temp']
        self.irms_dt = tech_params['layout']['em']['rms_dt']

    # NOTE: the following methods are still abstract (inherited from TechInfo):
    #
    #       get_metal_em_specs
    #       get_via_em_specs
    #       get_res_em_specs
    #       add_cell_boundary
    #       draw_device_blockage

    @abstractmethod
    def get_via_arr_enc(self,
                        vname: str,
                        vtype: str,
                        mtype: str,
                        mw_unit: int,
                        is_bot: bool) -> Tuple[
                                             Optional[List[Tuple[int, int]]],
                                             Optional[Callable[[int, int], bool]]
                                         ]:
        """TODO: description

        Parameters
        ----------
        vname : string
            the via type name.
        vtype : string
            the via type, square/hrect/vrect/etc.
        mtype : string
            name of the metal layer via is connecting.  Can be either top or bottom.
        mw_unit : int
            width of the metal, in resolution units.
        is_bot : bool
            True if the given metal is the bottom metal.
        """
        return None, None

    @property
    def pin_purpose(self) -> str:
        return self.config.get('pin_purpose', 'pin')

    def get_via_types(self,
                      bmtype: str,
                      tmtype: str) -> List[Tuple[str, int]]:
        default = [('square', 1), ('vrect', 2), ('hrect', 2)]
        if 'via_type_order' in self.config:
            table = self.config['via_type_order']
            return table.get((bmtype, tmtype), default)
        return default

    def get_well_layers(self, sub_type: str) -> List[Tuple[str, str]]:
        return self.config['well_layers'][sub_type]

    def get_implant_layers(self,
                           mos_type: str,
                           res_type: Optional[str] = None) -> List[Tuple[str, str]]:
        if res_type is None:
            table = self.config[self._mos_entry_name]
        else:
            table = self.config['resistor']

        return list(table['imp_layers'][mos_type].keys())

    def get_threshold_layers(self,
                             mos_type: str,
                             threshold: str,
                             res_type: Optional[str] = None) -> List[Tuple[str, str]]:
        if res_type is None:
            table = self.config[self._mos_entry_name]
        else:
            table = self.config['resistor']

        return list(table['thres_layers'][mos_type][threshold].keys())

    def get_exclude_layer(self, layer_id: int) -> Tuple[str, str]:
        """Returns the metal exclude layer"""
        return self.config['metal_exclude_table'][layer_id]

    def get_dnw_margin_unit(self, dnw_mode: str) -> int:
        return self.config['dnw_margins'][dnw_mode]

    def get_dnw_layers(self) -> List[Tuple[str, str]]:
        return self.config[self._mos_entry_name]['dnw_layers']

    def get_res_metal_layers(self, layer_id: int) -> List[Tuple[str, str]]:
        return self.config['res_metal_layer_table'][layer_id]

    def get_metal_dummy_layers(self, layer_id: int) -> List[Tuple[str, str]]:
        return self.config['metal_dummy_table'][layer_id]

    def use_flip_parity(self) -> bool:
        return self.config['use_flip_parity']

    def get_layer_name(self, layer_id: int) -> str:
        name_dict = self.config['layer_name']
        return name_dict[layer_id]

    def get_layer_id(self, layer_name: str) -> int:
        for key, val in self.config['layer_name'].items():
            if val == layer_name:
                return key
        raise ValueError('Unknown layer: %s' % layer_name)

    def get_layer_type(self, layer_name: str) -> str:
        type_dict = self.config['layer_type']
        return type_dict[layer_name]

    def get_idc_scale_factor(self,
                             temp: float,
                             mtype: str,
                             is_res: bool = False) -> float:
        if is_res:
            mtype = 'res'
        idc_em_scale = self.config['idc_em_scale']
        if mtype in idc_em_scale:
            idc_params = idc_em_scale[mtype]
        else:
            idc_params = idc_em_scale['default']

        temp_list = idc_params['temp']
        scale_list = idc_params['scale']

        for temp_test, scale in zip(temp_list, scale_list):
            if temp <= temp_test:
                return scale
        return scale_list[-1]

    def get_via_name(self, bot_layer_id: int) -> str:
        return self.config['via_name'][bot_layer_id]

    def get_via_id(self,
                   bot_layer: str,
                   top_layer: str) -> str:
        return self.config['via_id'][(bot_layer, top_layer)]

    def get_via_drc_info(self,
                         vname: str,
                         vtype: str,
                         mtype: str,
                         mw_unit: int,
                         is_bot: bool) -> Tuple[
                                              Tuple[int, int],                  # sp
                                              Optional[List[Tuple[int, int]]],  # sp2_list
                                              Optional[List[Tuple[int, int]]],  # sp3_list
                                              Optional[List[Tuple[int, int]]],  # sp6_list
                                              Tuple[int, int],                  # dim
                                              List[Tuple[int, int]],            # enc
                                              Optional[List[Tuple[int, int]]],  # arr_enc
                                              Optional[Callable]                # arr_test
                                          ]:
        via_config = self.config['via']
        if vname not in via_config:
            raise ValueError('Unsupported vname %s' % vname)

        via_config = via_config[vname]
        if vtype.startswith('vrect') and vtype not in via_config:
            # trying vertical rectangle via, but it does not exist,
            # so try rotating horizontal rectangle instead
            rotate = True
            vtype2 = 'hrect' + vtype[5:]
        else:
            rotate = False
            vtype2 = vtype
        if vtype2 not in via_config:
            raise ValueError('Unsupported vtype %s' % vtype2)

        via_config = via_config[vtype2]

        dim = via_config['dim']
        sp = via_config['sp']
        sp2_list = via_config.get('sp2', None)
        sp3_list = via_config.get('sp3', None)
        sp6_list = via_config.get('sp6', None)

        if not is_bot or via_config['bot_enc'] is None:
            enc_data = via_config['top_enc']
        else:
            enc_data = via_config['bot_enc']

        enc_w_list = enc_data['w_list']
        enc_list = enc_data['enc_list']

        enc_cur = []
        for mw_max, enc in zip(enc_w_list, enc_list):
            if mw_unit <= mw_max:
                enc_cur = enc
                break

        arr_enc, arr_test_tmp = self.get_via_arr_enc(vname, vtype, mtype, mw_unit, is_bot)
        arr_test = arr_test_tmp

        if rotate:
            sp = sp[1], sp[0]
            dim = dim[1], dim[0]
            enc_cur = [(yv, xv) for xv, yv in enc_cur]
            if sp2_list is not None:
                sp2_list = [(spy, spx) for spx, spy in sp2_list]
            if sp3_list is not None:
                sp3_list = [(spy, spx) for spx, spy in sp3_list]
            if sp6_list is not None:
                sp6_list = [(spy, spx) for spx, spy in sp6_list]
            if arr_enc is not None:
                arr_enc = [(yv, xv) for xv, yv in arr_enc]
            if arr_test_tmp is not None:
                def arr_test(nrow, ncol):
                    return arr_test_tmp(ncol, nrow)

        return sp, sp2_list, sp3_list, sp6_list, dim, enc_cur, arr_enc, arr_test

    def _space_helper(self,
                      config_name: str,
                      layer_type: str,
                      width: int) -> Optional[int]:
        sp_min_config = self.config[config_name]
        if layer_type not in sp_min_config:
            raise ValueError('Unsupported layer type: %s' % layer_type)

        sp_min_config = sp_min_config[layer_type]
        w_list = sp_min_config['w_list']
        sp_list = sp_min_config['sp_list']

        for w, sp in zip(w_list, sp_list):
            if width <= w:
                return sp
        return None

    def get_min_space_unit(self,
                           layer_type: str,
                           w_unit: int,
                           same_color: bool = False) -> Optional[int]:
        if not same_color or 'sp_sc_min' not in self.config:
            config_name = 'sp_min'
        else:
            config_name = 'sp_sc_min'

        return self._space_helper(config_name, layer_type, w_unit)

    def get_min_line_end_space_unit(self,
                                    layer_type: str,
                                    w_unit: int) -> Optional[int]:
        return self._space_helper('sp_le_min', layer_type, w_unit)

    def get_min_space(self,
                      layer_type: str,
                      width: Union[float, int],
                      unit_mode: bool = False,
                      same_color: bool = False) -> Union[float, int]:
        res = self.config['resolution']
        if not unit_mode:
            width = int(round(width / res))

        ans = self.get_min_space_unit(layer_type, width, same_color=same_color)

        if unit_mode:
            return ans
        return ans * res

    def get_min_line_end_space(self,
                               layer_type: str,
                               width: Union[float, int],
                               unit_mode: bool = False) -> Union[float, int]:
        res = self.config['resolution']
        if not unit_mode:
            width = int(round(width / res))

        ans = self.get_min_line_end_space_unit(layer_type, width)

        if unit_mode:
            return ans
        return ans * res

    def layer_id_to_type(self,
                         layer_id: int) -> str:
        name_dict = self.config['layer_name']
        type_dict = self.config['layer_type']
        return type_dict[name_dict[layer_id]]

    def get_min_length_unit(self,
                            layer_type: str,
                            w_unit: int) -> int:
        len_min_config = self.config['len_min']
        if layer_type not in len_min_config:
            raise ValueError('Unsupported layer type: %s' % layer_type)

        w_list = len_min_config[layer_type]['w_list']
        w_al_list = len_min_config[layer_type]['w_al_list']
        md_list = len_min_config[layer_type]['md_list']
        md_al_list = len_min_config[layer_type]['md_al_list']

        # get minimum length from width spec
        l_unit = 0
        for w, (area, len_min) in zip(w_list, w_al_list):
            if w_unit <= w:
                l_unit = max(len_min, -(-area // w_unit))
                break

        # check maximum dimension spec
        for max_dim, (area, len_min) in zip(reversed(md_list), reversed(md_al_list)):
            if max(w_unit, l_unit) > max_dim:
                return l_unit
            l_unit = max(l_unit, len_min, -(-area // w_unit))

        return -(-l_unit // 2) * 2

    def get_min_length(self,
                       layer_type: str,
                       width: float) -> float:
        res = self.resolution
        w_unit = int(round(width / res))
        return res * self.get_min_length_unit(layer_type, w_unit)

    def get_res_rsquare(self, res_type: str) -> float:
        return self.config['resistor']['info'][res_type]['rsq']

    def get_res_width_bounds(self, res_type: str) -> Tuple[float, float]:
        return self.config['resistor']['info'][res_type]['w_bounds']

    def get_res_length_bounds(self, res_type: str) -> Tuple[float, float]:
        return self.config['resistor']['info'][res_type]['l_bounds']

    def get_res_min_nsquare(self, res_type: str) -> float:
        return self.config['resistor']['info'][res_type]['min_nsq']
